## Homework 2:

---

### Tic-tac-toe

---

Реализовать игру крестики нолики с использованием алгоритма минимакс:

1. приложение должно иметь консольный интерфейс, на котором будет рисоваться игровое поле;
2. у игрока есть возможность задать размерность игрового поля в начале игры;
3. каждый ход игрок указывает номер клетки, в которую хочет поставить фигуру;
4. номер клетки по горизонтали и вертикали указывается от 1 до N;
5. код программы должен быть прост для чтения для человека, который знаком с алгоритмом минимакс:
    - блоки кода должны быть разделены на методы с читаемым наименованием;
    - блоки кода/методы должны иметь комментарии, кратко поясняющие логику программы.

---

**Помощь:**

1. [Реализация алгоритма Минимакс на примере игры «Крестики-Нолики»](https://habr.com/ru/post/329058/)
2. [Еще одна реализация с примерами на Java](https://youtube.com/watch?v=SGA9APhxQwk)
