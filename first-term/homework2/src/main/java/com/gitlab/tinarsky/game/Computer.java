package com.gitlab.tinarsky.game;

public class Computer extends Player {

    private static final int COMPUTER_WIN_POINTS = 1;
    private static final int USER_WIN_POINTS = -1;
    private static final int DRAW_POINTS = 0;
    private static final String COMPUTER_NAME = "Computer";

    public Computer(Symbol symbol, PlayingField playingField) {
        super(symbol, playingField);
    }

    @Override
    public String getName() {
        return COMPUTER_NAME;
    }

    @Override
    public void doMove(int currentTurnNumber) {
        super.doMove(currentTurnNumber);
        Position computerPositionToMove = getTheBestPositionToMove();
        playingField.setSymbol(symbol, computerPositionToMove);
    }

    private Position getTheBestPositionToMove() {
        Position bestPositionToMove = null;
        int maxEvaluation = Integer.MIN_VALUE;
        for (int i = 0; i < playingField.getSize(); i++) {
            for (int j = 0; j < playingField.getSize(); j++) {
                Position position = new Position(i, j);
                if (playingField.getSymbol(position) != Symbol.NONE) {
                    continue;
                }

                int evaluation = getEvaluationOfPosition(position, true);
                if (evaluation == COMPUTER_WIN_POINTS) {
                    return position;
                }

                if (evaluation > maxEvaluation) {
                    maxEvaluation = evaluation;
                    bestPositionToMove = position;
                }
            }
        }
        return bestPositionToMove;
    }

    private int getEvaluationOfPosition(Position position, boolean isComputerMove) {
        Symbol symbolOfPlayer = (isComputerMove ? getSymbol() : getOpponentSymbol());
        playingField.setSymbol(symbolOfPlayer, position);
        turnNumber++;
        int evaluation = getMinimaxEvaluation(!isComputerMove);
        playingField.setSymbol(Symbol.NONE, position);
        turnNumber--;
        return evaluation;
    }

    private int getMinimaxEvaluation(boolean isComputerMove) {
        if (playingField.didAnyoneWin()) {
            return (isComputerMove ? USER_WIN_POINTS : COMPUTER_WIN_POINTS);
        }

        if (turnNumber > playingField.getSize() * playingField.getSize()) {
            return DRAW_POINTS;
        }

        int evaluation = (isComputerMove ? USER_WIN_POINTS : COMPUTER_WIN_POINTS);
        for (int i = 0; i < playingField.getSize(); i++) {
            for (int j = 0; j < playingField.getSize(); j++) {
                Position position = new Position(i, j);
                if (playingField.getSymbol(position) != Symbol.NONE) {
                    continue;
                }

                if (isComputerMove) {
                    evaluation = Math.max(
                        evaluation,
                        getEvaluationOfPosition(position, true)
                    );
                    if (evaluation == COMPUTER_WIN_POINTS) {
                        return evaluation;
                    }
                    continue;
                }
                evaluation = Math.min(
                    evaluation,
                    getEvaluationOfPosition(position, false)
                );
                if (evaluation == USER_WIN_POINTS) {
                    return evaluation;
                }
            }
        }
        return evaluation;
    }
}
