package com.gitlab.tinarsky.input;

import com.gitlab.tinarsky.game.PlayingField;
import com.gitlab.tinarsky.game.Position;
import com.gitlab.tinarsky.game.Symbol;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class UserInputReader {

    private static final BufferedReader BUFFERED_READER =
        new BufferedReader(new InputStreamReader(System.in));

    public static int readPlayingFieldSize() {
        int playingFieldSize = 0;
        while (playingFieldSize == 0) {
            System.out.print("Enter playing field size: ");
            try {
                playingFieldSize = Integer.parseInt(BUFFERED_READER.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Playing field size must be a number!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return playingFieldSize;
    }

    public static boolean readIsUserMoveFirst() {
        System.out.println("Whose move is first?");
        while (true) {
            System.out.print("Enter 'u' (user) or 'c' (computer): ");
            String input;
            try {
                input = BUFFERED_READER.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            if (Objects.equals(input, "u")) {
                return true;
            }

            if (Objects.equals(input, "c")) {
                return false;
            }
            System.out.println("You should type either 'u' or 'c'");
        }
    }

    public static Position readUserMove(PlayingField playingField) {
        Position positionToMove = null;
        while (positionToMove == null) {
            System.out.print("Enter cell's coords separated by space (row and column): ");
            try {
                String[] input = BUFFERED_READER.readLine().split(" ");
                int x = Integer.parseInt(input[0]);
                if (isCoordinateNotValid(playingField, x)) {
                    System.out.println("Cell's coords must be the numbers from 1 to " +
                        playingField.getSize() + "!");
                    continue;
                }

                int y = Integer.parseInt(input[1]);
                if (isCoordinateNotValid(playingField, y)) {
                    System.out.println("Cell's coords must be the numbers from 1 to " +
                        playingField.getSize() + "!");
                    continue;
                }

                var position = new Position(x - 1, y - 1);
                if (playingField.getSymbol(position) != Symbol.NONE) {
                    System.out.println("This cell already has a symbol!");
                    continue;
                }
                positionToMove = position;
            } catch (NumberFormatException e) {
                System.out.println("Cell's coords must be the numbers!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return positionToMove;
    }

    private static boolean isCoordinateNotValid(PlayingField playingField, int coordinate) {
        return coordinate < 1 || coordinate > playingField.getSize();
    }
}
