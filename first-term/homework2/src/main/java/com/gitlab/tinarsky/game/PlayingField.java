package com.gitlab.tinarsky.game;

public class PlayingField {

    private static final String INCORRECT_POSITION_MESSAGE = "Incorrect coordinates of cell on the playing field!";
    private final Symbol[][] field;

    public PlayingField(int size) {
        field = new Symbol[size][size];
        initializeField();
    }

    private void initializeField() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = Symbol.NONE;
            }
        }
    }

    public Symbol getSymbol(Position position) {
        validatePosition(position);
        return field[position.x()][position.y()];
    }

    public void setSymbol(Symbol symbol, Position position) {
        validatePosition(position);
        field[position.x()][position.y()] = symbol;
    }

    /**
     * @param position position to validate
     * @throws IllegalArgumentException if position is invalid
     */
    public void validatePosition(Position position) {
        if (position.x() < 0 || position.x() >= field.length ||
            position.y() < 0 || position.y() >= field.length) {
            throw new IllegalArgumentException(INCORRECT_POSITION_MESSAGE);
        }
    }

    public int getSize() {
        return field.length;
    }

    public boolean didAnyoneWin() {
        boolean areSymbolsOnMainDiagonalSame = true;
        boolean areSymbolsOnSecondaryDiagonalSame = true;
        var firstSymbolOnMainDiagonal = getSymbol(new Position(0, 0));
        var firstSymbolOnSecondaryDiagonal = getSymbol(new Position(0, field.length - 1));
        for (int i = 0; i < field.length; i++) {
            boolean areSymbolsInRowSame = true;
            boolean areSymbolsInColumnSame = true;
            var firstSymbolInRow = getSymbol(new Position(i, 0));
            var firstSymbolInColumn = getSymbol(new Position(0, i));
            for (int j = 0; j < field.length; j++) {
                var symbolInRow = getSymbol(new Position(i, j));
                if (areSymbolsInRowSame) {
                    areSymbolsInRowSame = (symbolInRow == firstSymbolInRow);
                }

                var symbolInColumn = getSymbol(new Position(j, i));
                if (areSymbolsInColumnSame) {
                    areSymbolsInColumnSame = (symbolInColumn == firstSymbolInColumn);
                }
                
                if (i == j && areSymbolsOnMainDiagonalSame) {
                    areSymbolsOnMainDiagonalSame = (symbolInRow == firstSymbolOnMainDiagonal);
                }

                if (i == (field.length - 1 - j) && areSymbolsOnSecondaryDiagonalSame) {
                    areSymbolsOnSecondaryDiagonalSame = (symbolInRow
                        == firstSymbolOnSecondaryDiagonal);
                }
            }
            if (areSymbolsSameAndNotNone(areSymbolsInRowSame, firstSymbolInRow)) {
                return true;
            }

            if (areSymbolsSameAndNotNone(areSymbolsInColumnSame, firstSymbolInColumn)) {
                return true;
            }
        }

        if (areSymbolsSameAndNotNone(areSymbolsOnMainDiagonalSame, firstSymbolOnMainDiagonal)) {
            return true;
        }
        return areSymbolsSameAndNotNone(areSymbolsOnSecondaryDiagonalSame,
            firstSymbolOnSecondaryDiagonal);
    }

    private static boolean areSymbolsSameAndNotNone(boolean areSymbolsSame, Symbol firstSymbol) {
        return areSymbolsSame && firstSymbol != Symbol.NONE;
    }
}
