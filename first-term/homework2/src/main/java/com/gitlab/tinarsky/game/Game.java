package com.gitlab.tinarsky.game;

public class Game {

    private final PlayingField playingField;
    private final GameView gameView;
    private int turnNumber;
    private final User user;
    private final Computer computer;
    private Player activePlayer;

    public Game(int playingFieldSize, boolean isUserMoveFirst) {
        this.playingField = new PlayingField(playingFieldSize);
        this.gameView = new GameView(playingField);
        this.turnNumber = 1;
        if (isUserMoveFirst) {
            this.user = new User(Symbol.X, playingField);
            this.computer = new Computer(Symbol.O, playingField);
            this.activePlayer = user;
        } else {
            this.user = new User(Symbol.O, playingField);
            this.computer = new Computer(Symbol.X, playingField);
            this.activePlayer = computer;
        }
    }

    public void start() {
        gameView.updateView(null);
        while (true) {
            doPlayerMove(activePlayer);
            if (playingField.didAnyoneWin()) {
                gameView.printGameOverScreen(activePlayer);
                break;
            }

            if (isDraw()) {
                gameView.printGameOverScreen(null);
                break;
            }
            changeActivePlayer();
        }
    }

    private void doPlayerMove(Player player) {
        player.doMove(turnNumber);
        gameView.updateView(player);
        turnNumber++;
    }

    private boolean isDraw() {
        return turnNumber > playingField.getSize() * playingField.getSize();
    }

    private void changeActivePlayer() {
        if (activePlayer == user) {
            activePlayer = computer;
            return;
        }
        activePlayer = user;
    }
}
