package com.gitlab.tinarsky.game;

public class GameView {

    private final PlayingField playingField;

    public GameView(PlayingField playingField) {
        this.playingField = playingField;
    }

    /**
     * @param playerWhoseTurn player, whose turn it was. A null value is permitted, and indicates
     *                        that it is the initializing of view.
     */
    public void updateView(Player playerWhoseTurn) {
        printDashes();
        printTitle(playerWhoseTurn);
        System.out.print(" ");
        for (int i = 0; i < playingField.getSize(); i++) {
            System.out.print(" " + (i + 1));
        }
        System.out.println();
        for (int i = 0; i < playingField.getSize(); i++) {
            System.out.print(i + 1);
            for (int j = 0; j < playingField.getSize(); j++) {
                System.out.print(" " +
                    playingField.getSymbol(new Position(i, j)));
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * @param playerWhoWon player, who won in this game. A null value is permitted, and indicates
     *                     that the game ended in a draw.
     */
    public void printGameOverScreen(Player playerWhoWon) {
        printDashes();
        System.out.println("Game over!");
        if (playerWhoWon == null) {
            System.out.println("Draw");
            return;
        }
        System.out.println("Winner: " + playerWhoWon.getName());
    }

    private void printDashes() {
        System.out.println("-".repeat(playingField.getSize() * 5));
    }

    private static void printTitle(Player playerWhoseTurn) {
        if (playerWhoseTurn == null) {
            System.out.println("The game started!\n");
        } else {
            System.out.println("Turn of " + playerWhoseTurn.getName() + "\n");
        }
    }
}
