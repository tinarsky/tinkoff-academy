package com.gitlab.tinarsky.game;

public record Position(int x, int y) {

}
