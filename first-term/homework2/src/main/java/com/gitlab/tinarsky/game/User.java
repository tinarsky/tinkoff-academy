package com.gitlab.tinarsky.game;

import com.gitlab.tinarsky.input.UserInputReader;

public class User extends Player {

    private static final String USER_NAME = "User";

    public User(Symbol symbol, PlayingField playingField) {
        super(symbol, playingField);
    }

    @Override
    public String getName() {
        return USER_NAME;
    }

    @Override
    public void doMove(int currentTurnNumber) {
        Position userPositionToMove =
            UserInputReader.readUserMove(playingField);
        playingField.setSymbol(symbol, userPositionToMove);
    }
}
