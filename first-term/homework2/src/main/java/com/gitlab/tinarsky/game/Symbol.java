package com.gitlab.tinarsky.game;

public enum Symbol {
    X, O, NONE;

    @Override
    public String toString() {
        if (this == NONE) {
            return " ";
        }
        return name();
    }
}
