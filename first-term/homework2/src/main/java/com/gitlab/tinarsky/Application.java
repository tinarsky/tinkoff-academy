package com.gitlab.tinarsky;

import com.gitlab.tinarsky.game.Game;
import com.gitlab.tinarsky.input.UserInputReader;

public class Application {

    public static void main(String[] args) {
        Game game = initializeAndGetGame();
        game.start();
    }

    public static Game initializeAndGetGame() {
        int size = UserInputReader.readPlayingFieldSize();
        boolean isUserMoveFirst = UserInputReader.readIsUserMoveFirst();
        return new Game(size, isUserMoveFirst);
    }
}
