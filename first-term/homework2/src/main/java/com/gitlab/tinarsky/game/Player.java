package com.gitlab.tinarsky.game;

public abstract class Player {

    protected final Symbol symbol;
    protected final PlayingField playingField;
    protected int turnNumber;

    public Player(Symbol symbol, PlayingField playingField) {
        this.symbol = symbol;
        this.playingField = playingField;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public Symbol getOpponentSymbol() {
        if (symbol == Symbol.X) {
            return Symbol.O;
        }
        return Symbol.X;
    }

    public void doMove(int currentMoveNumber) {
        this.turnNumber = currentMoveNumber;
    }

    public abstract String getName();
}
