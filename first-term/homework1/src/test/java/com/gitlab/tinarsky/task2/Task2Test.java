package com.gitlab.tinarsky.task2;

import static com.gitlab.tinarsky.task2.Task2.countDuplicates;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import org.junit.Test;

public class Task2Test {

    @Test
    public void testCountDuplicates() {
        assertEquals(Map.of(1, 4L), (countDuplicates(List.of(1, 1, 1, 1))));
        assertEquals(Map.of(), countDuplicates(List.of(1, 2, 3, 4)));
        assertEquals(Map.of(11, 2L), countDuplicates(List.of(11, 20, 1, 11)));
        assertEquals(Map.of(57, 2L, 100, 2L), countDuplicates(List.of(57, 100, 100, 57, 10)));
        assertEquals(Map.of(), countDuplicates(List.of()));
    }
}
