package com.gitlab.tinarsky.task1;

import static com.gitlab.tinarsky.task1.Task1.square56;
import static org.junit.Assert.assertEquals;

import java.util.List;
import org.junit.Test;

public class Task1Test {

    @Test
    public void testSquare56() {
        assertEquals(List.of(19, 11), square56(List.of(3, 1, 4)));
        assertEquals(List.of(11), square56(List.of(1)));
        assertEquals(List.of(14), square56(List.of(2)));
        assertEquals(List.of(), square56(List.of(5, 6, 14)));
        assertEquals(List.of(), square56(List.of()));
    }
}
