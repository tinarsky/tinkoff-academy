package com.gitlab.tinarsky.task4;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Test;

public class Task4Test {

    @Test
    public void testPrint123SixTimes() {
        var standardOut = System.out;
        var byteArrayOutputStream = new ByteArrayOutputStream();
        var customOut = new PrintStream(byteArrayOutputStream);

        System.setOut(customOut);
        SequencePrinter.print123SixTimes();
        customOut.flush();
        System.setOut(standardOut);

        assertEquals("123".repeat(6), byteArrayOutputStream.toString());
    }

    @Test
    public void testPrint123SixTimesTwice() {
        var standardOut = System.out;
        var byteArrayOutputStream = new ByteArrayOutputStream();
        var customOut = new PrintStream(byteArrayOutputStream);

        System.setOut(customOut);
        SequencePrinter.print123SixTimes();
        SequencePrinter.print123SixTimes();
        customOut.flush();
        System.setOut(standardOut);

        assertEquals("123".repeat(12), byteArrayOutputStream.toString());
    }
}
