package com.gitlab.tinarsky.task3.models;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ProductSupplier implements Runnable {

    private final Store store;
    private final String name;
    private static final AtomicInteger counter = new AtomicInteger(1);
    private final Random random;
    private final int timeoutMin;
    private final int timeoutMax;

    public ProductSupplier(Store store, int timeoutMin, int timeoutMax) {
        this.store = store;
        this.name = "Supplier number " + counter.getAndIncrement();
        this.random = new Random();
        this.timeoutMin = timeoutMin;
        this.timeoutMax = timeoutMax;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                TimeUnit.SECONDS.sleep(random.nextLong(timeoutMin, timeoutMax));
                supplyToStore();
            }
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted");
        }
    }

    private void supplyToStore() throws InterruptedException {
        var product = new Product();
        store.supplyProduct(product);
        System.out.println(product + " was supplied by " + this.name);
    }
}
