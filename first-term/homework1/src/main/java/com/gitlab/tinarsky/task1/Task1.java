package com.gitlab.tinarsky.task1;

import java.util.List;

public class Task1 {

    public static List<Integer> square56(List<Integer> list) {
        return list.stream()
            .map(num -> num * num + 10)
            .filter(num -> num % 10 != 5 && num % 10 != 6)
            .toList();
    }
}
