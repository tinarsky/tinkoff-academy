package com.gitlab.tinarsky.task3.models;

import java.util.concurrent.atomic.AtomicInteger;

public class Product {

    private final String name;
    private static final AtomicInteger counter = new AtomicInteger(1);

    public Product() {
        this.name = "Product number " + counter.getAndIncrement();
    }

    @Override
    public String toString() {
        return name;
    }
}
