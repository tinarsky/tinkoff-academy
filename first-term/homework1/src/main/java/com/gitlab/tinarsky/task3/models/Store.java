package com.gitlab.tinarsky.task3.models;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Store {

    private final BlockingQueue<Product> products;

    public Store(int capacity) {
        this.products = new ArrayBlockingQueue<>(capacity, true);
    }

    public void supplyProduct(Product product) throws InterruptedException {
        products.put(product);
    }

    public Product consumeProduct() throws InterruptedException {
        return products.take();
    }
}
