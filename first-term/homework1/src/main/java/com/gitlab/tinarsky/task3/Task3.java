package com.gitlab.tinarsky.task3;

import static java.util.concurrent.Executors.newCachedThreadPool;

import com.gitlab.tinarsky.task3.models.ProductConsumer;
import com.gitlab.tinarsky.task3.models.ProductSupplier;
import com.gitlab.tinarsky.task3.models.Store;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Task3 {

    private static int appRunTime;
    private static int suppliersQuantity;
    private static int consumersQuantity;
    private static int storeCapacity;
    private static int suppliersTimeoutMin;
    private static int suppliersTimeoutMax;

    private static void loadProperties() {
        Properties properties = new Properties();
        String resourcePath = Thread.currentThread()
            .getContextClassLoader()
            .getResource("task3/task3.properties").getPath();
        try (var reader = new FileReader(resourcePath)) {
            properties.load(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        appRunTime =
            Integer.parseInt(properties.getProperty("app.run.time.seconds"));
        suppliersQuantity =
            Integer.parseInt(properties.getProperty("suppliers.quantity"));
        consumersQuantity =
            Integer.parseInt(properties.getProperty("consumers.quantity"));
        storeCapacity =
            Integer.parseInt(properties.getProperty("store.capacity"));
        suppliersTimeoutMin =
            Integer.parseInt(properties.getProperty("suppliers.timeout.min"));
        suppliersTimeoutMax =
            Integer.parseInt(properties.getProperty("suppliers.timeout.max"));
    }

    public static void main(String[] args) throws InterruptedException {
        loadProperties();

        var store = new Store(storeCapacity);
        ExecutorService threadPool = newCachedThreadPool();

        Stream.generate(() -> new ProductSupplier(store, suppliersTimeoutMin, suppliersTimeoutMax))
            .limit(suppliersQuantity)
            .forEach(threadPool::execute);

        Stream.generate(() -> new ProductConsumer(store))
            .limit(consumersQuantity)
            .forEach(threadPool::execute);

        TimeUnit.SECONDS.sleep(appRunTime);

        threadPool.shutdownNow();
    }
}
