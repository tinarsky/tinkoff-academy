package com.gitlab.tinarsky.task4;

import java.util.List;
import java.util.stream.IntStream;

public class SequencePrinter {

    private static final int TARGET_NUMBER_OF_REPETITIONS = 6;
    private static final int LAST_NUMBER_IN_SEQUENCE = 3;
    private static final Object MONITOR_FOR_PRINTING_NUMBERS = new Object();
    private static final Object MONITOR_FOR_PUBLIC_METHODS = new Object();
    private static int numberOfRepetitions;
    private static int lastPrintedNumber;

    private SequencePrinter() {
    }

    /**
     * Method synchronises on private monitor. Therefore, only one thread at the same time can
     * execute it.
     */
    public static void print123SixTimes() {
        synchronized (MONITOR_FOR_PUBLIC_METHODS) {
            restoreInitialState();
            List<Thread> threads = IntStream.range(1, LAST_NUMBER_IN_SEQUENCE + 1)
                .mapToObj(num -> new Thread(
                    () -> printNumberSynchronized(num, getNumberThatShouldPrecede(num))))
                .toList();
            threads.forEach(Thread::start);
            for (Thread thread : threads) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private static int getNumberThatShouldPrecede(int num) {
        return num - 1 == 0 ? LAST_NUMBER_IN_SEQUENCE : num - 1;
    }

    private static void printNumberSynchronized(int number, int numberThatShouldPrecede) {
        while (numberOfRepetitions != TARGET_NUMBER_OF_REPETITIONS) {
            synchronized (MONITOR_FOR_PRINTING_NUMBERS) {
                while (lastPrintedNumber != numberThatShouldPrecede
                    && numberOfRepetitions != TARGET_NUMBER_OF_REPETITIONS) {
                    try {
                        MONITOR_FOR_PRINTING_NUMBERS.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (numberOfRepetitions == TARGET_NUMBER_OF_REPETITIONS) {
                    break;
                }

                if (number == LAST_NUMBER_IN_SEQUENCE) {
                    numberOfRepetitions++;
                }
                System.out.print(number);
                lastPrintedNumber = number;
                MONITOR_FOR_PRINTING_NUMBERS.notifyAll();
            }
        }
    }

    private static void restoreInitialState() {
        numberOfRepetitions = 0;
        lastPrintedNumber = LAST_NUMBER_IN_SEQUENCE;
    }
}
