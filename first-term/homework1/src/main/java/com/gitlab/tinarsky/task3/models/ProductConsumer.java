package com.gitlab.tinarsky.task3.models;

import java.util.concurrent.atomic.AtomicInteger;

public class ProductConsumer implements Runnable {

    private final Store store;
    private final String name;
    private static final AtomicInteger counter = new AtomicInteger(1);

    public ProductConsumer(Store store) {
        this.store = store;
        this.name = "Consumer number " + counter.getAndIncrement();
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                consumeFromStore();
            }
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted");
        }
    }

    private void consumeFromStore() throws InterruptedException {
        var product = store.consumeProduct();
        System.out.println(product + " was consumed by " + this.name);
    }
}
